module.exports = {
    networks: {
        develop: {
            host: "127.0.0.1",
            port: 9545,
            network_id: "1", // Match any network id
            gas: 8700000
        },
        development: {
            host: "127.0.0.1",
            port: 9545,
            network_id: "1", // Match any network id
            gas: 8700000
        }
    }
};