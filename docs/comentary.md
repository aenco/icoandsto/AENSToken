Dear Aenco Community, 
As a socially responsible company, Aenco sees the safeguarding of token utility and user safety as our prime values, therefore to maintain your trust we are enacting timely positive changes.

Thanks to the confidence and support from our communities, partners and investors, Aenco is thrilled to announce the community migration of AEN tokens with a technologically-advanced token contract upgraded as AenSmart (AENS).

The upgraded AENS tokens showcases technology and user safety fortification, mainly in the strengthened technical features and amplified protective measures to token holders.

The AENS tokens comprehend a substantial shift from the computation of Ethereum version 4 to Ethereums version 5, enabling upgrades in the technical operations and security of the utility token. The new AENS smart contract is coded with the latest add-on capacities pertaining to protecting token holders and the tokens operational capabilities to facilitate compliance evaluation, business operations and market flexibility.

All current AEN token holders are eligible to receive a one-for-one token swap to AENS token through a scheduled airdrop on 10 June 2020 at GMT 12:30 AM (TBD). The new AENS tokens will be reflected in the same wallet holding AEN tokens.

We invite our communities to check their existing AEN tokens’ holding wallets for the new AENS tokens; and should you have any problems seeing them, kindly approach our customer support team at moon@aencoin.com.

As before, the AENS tokens are tradable on LAtoken and Digifinex and any existing tokens you have on those exchanges if withdrawn will be using the new AENS Token. Likewise, there will be no interruption in the token’s current operational features and company issuer, whereas the supply remains the same, i.e. 4,000,000,000 AEN.

If you would like to inquire further into any details of the new AENS Token, kindly email us at moon@aencoin.com.

Best regards,
The Aenco Team