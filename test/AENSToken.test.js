const AENSToken = artifacts.require('./AENSToken.sol'); //must be the name of the contract file
const suite = require('../test/erc20suite.js');
const expect = require('chai')
    .use(require('chai-bn')(web3.utils.BN))
    .expect


/* ===============================================================================================
*   persistent contract to run all the tests. instance is checked for deployment before every it
*  -----------------------------------------------------------------------------------------------*/
contract("AENSToken-Check Default Values", function (accounts) {
    let instance;
    beforeEach('Initialise the Contract before each test', async function () { // before each it()
        instance = await AENSToken.new();
    });
    it("Has an initial supply of: 1,000,000,000.00000000", async () => {
        var supply = await instance.totalSupply();
        assert.equal(supply.toString(), "100000000000000000", "Total Supply is incorrect");
        var balance = await instance.balanceOf(accounts[0]);
        assert.equal(balance.toString(), "100000000000000000", "Account 0 Balance  is incorrect");
    });
    it("Checking Initial Rules should be: 0x00 [Can receive and send, no whitelist to send]", async () => {
        assert.equal(await instance.getWhitelistRule(), 0x00, "White List rule default is set wrong");
    });

    it("No one whitelisted so a send should fail [200]", async () => {
        assert.equal(await instance.detectTransferRestriction(accounts[1],accounts[0], 0 ) , 0, "White list test return bad logic");
    });
});

contract("AENSToken-Testing ERC20 Compatibility", function (accounts) {
    let instance;
    let options = {
        accounts: accounts, // accounts to test with, accounts[0] being the contract owner
        create: async function () {   // factory method to create new token contract
            return await AENSToken.new();
        },
        transfer: async function (token, to, amount) {
            return await token.transfer(to, amount, { from: accounts[0] });
        },
        name: 'Smart Pharmaceutical Token',
        symbol: 'SMPT',
        decimals: '8',
        initialSupply: 100000000000000000,
        initialBalances: [
         [accounts[0], 100000000000000000]
        ],
        initialAllowances: [
            [accounts[0],accounts[1], 0]
        ]
    };

    beforeEach('Initialise the Contract before each test', async function () { // before each it()
        instance = await AENSToken.new();
        await instance.setWhitelistRule("0x00" );
    });
    describe('Basic ERC20 Audit', async () => {
        it("White list rule removed 0x00", async () => {
            assert.equal(await instance.getWhitelistRule(), 0x00, "White List rule default is set wrong");
        });
        it("Begin ERC20 Audit", async () => {
            suite(options);
        });
    });
});

contract("Checking With Receive Rule Set", function (accounts) {
    let instance;
    /*  0x00 = No rule
        0x01 = Receiver must be Listed
        0x10 = Sender must be listed
        0x11 = Both must be listed
    */
    beforeEach('Initialise the Contract with receive Rule', async function () { // before each it()
        instance = await AENSToken.new();
        await instance.setWhitelistRule("0x01" );
    });

    it("Checking Initial Rules should be: 0x01 [Can receive and send, no whitelist to send]", async () => {
        assert.equal(await instance.getWhitelistRule(), 0x01, "White List rule default is set wrong");
    });
    it("Has an initial supply of: 1,000,000,000.00000000", async () => {
        var supply = await instance.totalSupply();
        assert.equal(supply.toString(), "100000000000000000", "Total Supply is incorrect");
        var balance = await instance.balanceOf(accounts[0]);
        assert.equal(balance.toString(), "100000000000000000", "Account 0 Balance  is incorrect");
    });
    it("No one whitelisted Checking restriction result!", async () => {
        assert.equal(await instance.detectTransferRestriction(accounts[1],accounts[0], 0 ) , 2, "White list receiver return wrong code!");
        assert.equal(await instance.messageForTransferRestriction(await instance.detectTransferRestriction(accounts[1],accounts[0], 0)) , "Receiving address has not been KYC Approved", "Fail code returns wrong string!");
    });
    it("Try to transfer to a none whitelisted Account and Fail correctly", async () => {
        await expectRevertOrFail(instance.transfer(accounts[1], 1000000));
        var balance = await instance.balanceOf(accounts[1]);
        expect(balance).to.be.bignumber.equal(toBigNumber(0));
    });

    it("Add Manager", async () => {
        await instance.addManager(accounts[1], { from: accounts[0] });
        assert.isTrue(await instance.managers(accounts[1], { from: accounts[0] }),"Manager should be Present");
    });

    describe('Manager Added Accounts', async () => {
        beforeEach('Initialise with the Manager and whitelist', async function () { // before each it()
            await instance.addManager(accounts[1], {from: accounts[0]});
            await instance.addToReceiveAllowed(accounts[2], {from: accounts[1]});
            assert.equal(await instance.whiteList(accounts[2], {from: accounts[0]}), "0x01", "Account Should be whitelisted!");
        });
        it("Transfer to whitelisted Account", async () => {
            assert.equal(await instance.getWhitelistRule(), 0x01, "White List rule default is set wrong");
            await instance.transfer(accounts[2], 1000000);
            var balance = await instance.balanceOf(accounts[2]);
            expect(balance).to.be.bignumber.equal(toBigNumber(1000000));
        });
        it("Manager Should not be able to Send", async () => {
            await expectRevertOrFail(instance.transfer(accounts[1], 5000000,{from: accounts[0]}));
            var balance = await instance.balanceOf(accounts[1]);
            expect(balance).to.be.bignumber.equal(toBigNumber(0));
        });
    });
});


contract("Checking With Send Rule Set", function (accounts) {
    let instance;
    /*  0x00 = No rule [0000]
        0x01 = Receiver must be Listed [0001]
        0x02 = Sender must be listed [0010]
        0x03 = Both must be listed [0011]
    */
    beforeEach('Initialise the Contract with receive Rule', async function () { // before each it()
        instance = await AENSToken.new();
        await instance.setWhitelistRule("0x02");
    });

    it("Checking Initial Rules should be: 0x02 [Can receive and send, no whitelist to send]", async () => {
        assert.equal(await instance.getWhitelistRule(), 0x02, "White List rule default is set wrong");
    });
    // it("Has an initial supply of: 1,000,000,000.00000000", async () => {
    //     var supply = await instance.totalSupply();
    //     assert.equal(supply.toString(), "100000000000000000", "Total Supply is incorrect");
    //     var balance = await instance.balanceOf(accounts[0]);
    //     assert.equal(balance.toString(), "100000000000000000", "Account 0 Balance  is incorrect");
    // });
    // it("No one whitelisted Checking restriction result!", async () => {
    //     assert.equal(await instance.detectTransferRestriction(accounts[1], accounts[0], 0), 2, "White list receiver return wrong code!");
    //     assert.equal(await instance.messageForTransferRestriction(await instance.detectTransferRestriction(accounts[1], accounts[0], 0)), "Receiving address has not been KYC Approved", "Fail code returns wrong string!");
    // });
    // it("Try to transfer to a none whitelisted Account and Fail correctly", async () => {
    //     await expectRevertOrFail(instance.transfer(accounts[1], 1000000));
    //     var balance = await instance.balanceOf(accounts[1]);
    //     expect(balance).to.be.bignumber.equal(toBigNumber(0));
    // });
    //
    // it("Add Manager", async () => {
    //     await instance.addManager(accounts[1], {from: accounts[0]});
    //     assert.isTrue(await instance.managers(accounts[1], {from: accounts[0]}), "Manager should be Present");
    // });
    //
    // describe('Manager Added Accounts', async () => {
    //     beforeEach('Initialise with the Manager and whitelist', async function () { // before each it()
    //         await instance.addManager(accounts[1], {from: accounts[0]});
    //         await instance.addToReceiveAllowed(accounts[2], {from: accounts[1]});
    //         assert.equal(await instance.whiteList(accounts[2], {from: accounts[0]}), "0x01", "Account Should be whitelisted!");
    //     });
    //     it("Transfer to whitelisted Account", async () => {
    //         assert.equal(await instance.getWhitelistRule(), 0x01, "White List rule default is set wrong");
    //         await instance.transfer(accounts[2], 1000000);
    //         var balance = await instance.balanceOf(accounts[2]);
    //         expect(balance).to.be.bignumber.equal(toBigNumber(1000000));
    //     });
    //     it("Manager Should not be able to Send", async () => {
    //         await expectRevertOrFail(instance.transfer(accounts[1], 5000000, {from: accounts[0]}));
    //         var balance = await instance.balanceOf(accounts[1]);
    //         expect(balance).to.be.bignumber.equal(toBigNumber(0));
    //     });
    // });
});

// - - - - - - - - - - - - - - - - Some foot notes - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//     /*
//     * TODo: Manager to remove white list accounts
//     * TODo: Manager to add manager [fail]
//     * TODo: Manager to remove manager [fail]
//     * TODo: Manager to Freeze account [fail]

//     * TODo: Add to white list
//     * TODo: Send to none white list
//     * TODo: Receive

//     * TODo: Add second to white list
//     * TODo: Send to none white list
//     * TODo: Send to white list
//     * TODo: Receive
//     * TODo: Contract section with sender restriction
//     * TODO: Contract section with Receiver restriction
//     * TODO: Contract section with Both restriction
//     * TODO Contract section with Freeze and Unfreeze
//     * TODO: Add accounts to whitelist
//     * */
// await expectRevertOrFail(await instance.transfer(accounts[1] , 10000),"This should Fail", { from: from });
// assert.equal( await instance.transfer(accounts[1] , 10000) ,"This should fail" );
// console.log("Catch the fail", await instance.transfer(accounts[1] , 10000) );
// console.log("Balance should be Zero:",await instance.balanceOf(accounts[1]));
// assert.equal(await instance.getWhitelistRule(), 0x01, "White List rule default is set wrong");
// await instance.transfer(accounts[1], 1000000);





/* ------------------------------------------------------------------
*
*
* */

/**
 * Asserts that given promise will throw because of revert().
 * @param {Promise} promise
 */
async function expectRevert(promise) {
    await expectError(promise, ['revert'])
}

/**
 * Asserts that given promise will throw because of revert() or failed assertion.
 * @param {Promise} promise
 */
async function expectRevertOrFail(promise) {
    await expectError(promise, ['revert', 'invalid opcode'])
}

/**
 * Asserts that given promise will throw and that thrown message will contain one of the given
 * search strings.
 *
 * @param {Promise} promise The promise expecting to throw.
 * @param {string[]} messages List of expected thrown message search strings.
 */
async function expectError(promise, messages) {
    try {
        await promise
    } catch (error) {
        for (let i = 0; i < messages.length; i++) {
            if (error.message.search(messages[i]) >= 0) {
                return
            }
        }
        assert.fail("Expected revert, got '" + error + "' instead.")
    }
    assert.fail('Expected revert not received.')
}


/**
 * Converts given value to BigNumber object if it is number or string. Otherwise defaultValue is
 * returned in case given value is not truthy.
 *
 * @param {number|string|BigNumber|null} number
 * @param {number|string|BigNumber|null} [defaultValue]
 * @returns {BigNumber|null}
 */
function toBigNumber(number, defaultValue = null) {
    if (typeof number === 'string' || typeof number === 'number') {
        return new web3.utils.BN(number.toString())
    }
    else if (number) {
        return number
    }
    else if (defaultValue == null) {
        return null
    }
    else {
        return new web3.utils.BN(defaultValue)
    }
}
